import { createStore } from 'vuex'
import VuexPersister from 'vuex-persister'

const vuexPersister = new VuexPersister({
  key: 'my_key6',
  overwrite: true,
  storage: sessionStorage
})

export default createStore({

  state: {
    counter:1,
    items:[
      {
        id:0,
        title: "Item 1",
        body: "Item 1 description",
        important: false
      }
    ]
  },

  getters: {
    getItems(state){
      return state.items;
    }
  },

  mutations: {
    addItem(state, value){
      state.items.push(value);
      state.counter ++;
    },
    removeItem(state, itemId) {
      const index = state.items.findIndex(item => item.id === itemId);
      if (index !== -1) {
        state.items.splice(index, 1);
      }
    },
    modifyItem(state, { itemId, newItemWithId }) {
      const index = state.items.findIndex(item => item.id === itemId);

      if (index !== -1) {
        state.items.splice(index, 1, newItemWithId);
        console.log(state.items[index])
      }
    }
  },

  actions: {
    addItem({ commit, state }, newItem) {
      const newItemWithId = {
        ...newItem,
        id: state.counter
      };
      commit('addItem', newItemWithId);
    },
    removeItem({ commit }, itemId) {
      commit('removeItem', itemId);
    },
    modifyItem({ commit }, { itemId, updatedItem }) {
      const newItemWithId = {
        ...updatedItem.value,
        id: itemId
      };
      commit('modifyItem', { itemId, newItemWithId });
    }
  },

  modules: {
  },

  plugins: [vuexPersister.persist]
})
