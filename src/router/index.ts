import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import ListView from '../views/ListView.vue'
import ItemView from '../views/ItemView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    redirect: '/list'
  },
  {
    path: '/list',
    name: 'list',
    component: ListView
  },
  {
    path: '/item/:id',
    name: 'item',
    component: ItemView,
    props: true
  },
  {
    path: '/item/',
    name: 'additem',
    component: ItemView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
