# QUANTIAPP

Created by Benedek Molnár on 18.11.2023.
Realization of CRUD App based on the task below.
Pull the repo and use the commands below to set up the project.

## Project task

Build a CRUD application using Vue, the application needs to use Vuex and Vue Router. There will
be two routes – list and detail. The application should be able to:
    * create an item
    * show a list of all items
    * show a detail of an item on a separate route (such as /item/{id})
    * item can be edited when the detail is shown
    * item can be deleted from the list view
    * items can be sorted by title (ascending and descending)
The item will include a title, a body and a checkbox that will mark it as important when checked.
The title and the body are required and their values must to be validated before the item is added
or edited. In the list view, important items will have a different style than the normal ones.
Use BootStrap Vue or any other UI library to style the application.
Include a README.md with instructions on how to install/run the application. Publish on github

## Project Setup

```sh
yarn
```

### Compile and Hot-Reload for Development

```sh
yarn dev
```

### Type-Check, Compile and Minify for Production

```sh
yarn build
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn lint
```
